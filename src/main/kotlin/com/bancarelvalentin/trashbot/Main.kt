package com.bancarelvalentin.trashbot

import com.bancarelvalentin.ezbot.Orchestrator
import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.trashbot.background.garbagequotes.GarbageQuoteBot
import com.bancarelvalentin.trashbot.background.nap.NapBot
import com.bancarelvalentin.trashbot.command.talk.TalkBotTextCommand

// https://discord.com/oauth2/authorize?client_id=773565897654665237&scope=bot&permissions=8
class Main {


    companion object {

        @JvmStatic
        fun main(args: Array<String>) {
            ConfigHandler.config = TrashConfig()
            Orchestrator.add(GarbageQuoteBot())
            Orchestrator.add(TalkBotTextCommand())
            Orchestrator.add(NapBot())
            Orchestrator.start()
        }

    }

}
