package com.bancarelvalentin.trashbot

import com.bancarelvalentin.ezbot.config.Config
import com.bancarelvalentin.ezbot.config.EnvConfig
import java.util.*

class TrashConfig : Config {

    // Main
    override val language = Locale.ENGLISH
    override val extraI18nProjectTokens = arrayOf("e7314e46e8e1474691ae14cc99085aab")
    override val versionNumber: String
        get() = Version().toString()


    // Specific channels
    override val errorLogsChannelId = 931230763893006366

    // Feature flags
    override val supportLazyCommands = false
    override val djChannelId = if (EnvConfig.DEV) 955180385380225054 else 955949732818215013
    override val supportDjPlayerCommands = true
    override val supportDjReactionPlayer = true
    override val logErrorsOnSentry = true
    override val timeStartupInSentry = true
    override val timeCommandExecutionsInSentry = true
    override val timeCronExecutionsInSentry = true
    override val createI18nOnBoot = EnvConfig.DEV

    // Access rights
    override val moderatorsIds = arrayOf(427795896496029716, 443750643375800320)// Trash and me
    override val moderatorsRolesIds = arrayOf(770732667137425448)// Bob
    override val defaultWhitelistedGuildIds = arrayOf(if (EnvConfig.DEV) 677258457778356237 else 477581594815627265)


}
