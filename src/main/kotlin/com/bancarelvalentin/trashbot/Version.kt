package com.bancarelvalentin.trashbot

import com.bancarelvalentin.ezbot.logger.LogCategoriesEnum
import com.bancarelvalentin.ezbot.logger.Logger
import org.apache.commons.io.IOUtils


class Version {

    private val version = try {
        val resourceAsStream = this.javaClass.getResourceAsStream("/version.txt")
        IOUtils.readLines(resourceAsStream).joinToString { it as String }
    } catch (e: Exception) {
        Logger(this.javaClass).error("Can't get version", LogCategoriesEnum.DEFAULT, e)
        "[ERROR]"
    }

    override fun toString(): String {
        return version
    }
}