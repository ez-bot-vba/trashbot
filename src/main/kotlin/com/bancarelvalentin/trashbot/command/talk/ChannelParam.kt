package com.bancarelvalentin.trashbot.command.talk

import com.bancarelvalentin.ezbot.process.command.param.ChannelCommandParam

class ChannelParam : ChannelCommandParam() {
    override val rawName = "Channel"
    override val rawDesc = "Channel in wich send the message"
}