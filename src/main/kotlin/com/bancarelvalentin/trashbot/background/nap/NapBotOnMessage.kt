package com.bancarelvalentin.trashbot.background.nap

import com.bancarelvalentin.ezbot.config.EnvConfig
import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.Process
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import kotlin.math.ceil
import kotlin.math.floor


class NapBotOnMessage(process: Process) : EventListener(process) {
    private val quotes = arrayOf(
        "I'm napping ! What do you want ?",
        "Please stop pinging me or asking me stuff I'm just a trash bot I can't do anything without Teddy !",
        "You pinged me ? How may i help ? Just kidding i'm going back to sleep. Bye."
    )

    override fun onMessageReceived(event: MessageReceivedEvent) {
        val oneOutOfFifteen = ceil(Math.random().times(15)).equals(15.0)
        if (event.message.isMentioned(event.jda.selfUser) && (oneOutOfFifteen || EnvConfig.DEV)) {
            logger.trace("Answering to a ping with a random quote")
            val rndQuoteIndex = floor(Math.random().times(quotes.size)).toInt()
            event.message.reply(quotes[rndQuoteIndex]).complete()
        }
    }
}

