package com.bancarelvalentin.trashbot.background.nap


import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.background.BackgroundProcess


class NapBot : BackgroundProcess() {
    override val rawDesc = "Answer when bot is pinged"
    override val rawName = "Nap bot"

    override fun getListeners(): Array<EventListener> {
        return arrayOf(NapBotOnMessage(this))
    }
}