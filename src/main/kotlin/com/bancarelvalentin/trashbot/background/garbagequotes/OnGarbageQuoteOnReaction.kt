package com.bancarelvalentin.trashbot.background.garbagequotes

import com.bancarelvalentin.ezbot.config.EnvConfig
import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.Process
import com.bancarelvalentin.ezbot.utils.ColorUtils
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent
import java.io.BufferedInputStream
import java.net.URL


class OnGarbageQuoteOnReaction(process: Process) : EventListener(process) {


    // The emote we are looking for (only one of those must be set)
    private val REACTION_UNICODE = null
    private val REACTION_TEXT = if (EnvConfig.DEV) "testaus" else "a0_top500"

    // The min amount of reactions to pin the message to the museum
    private val REACTION_COUNT = if (EnvConfig.DEV) 1 else 3

    // The id of the museum channel id
    private val GARBAGE_MUSEUM = if (EnvConfig.DEV) "788223412896137236" else "773565020113469500"

    override fun onMessageReactionAdd(event: MessageReactionAddEvent) {
        val emoji = event.reactionEmote
        val channel = gateway.getTextChannelById(event.channel.id)
        if (channel == null || channel.permissionOverrides.size > 0) return
        if (!emoji.isEmote || emoji.emote.name != REACTION_TEXT) return

        val raw = emoji.emote.name
        logger.trace("New reaction event with $raw - Wanted reaction is ${REACTION_UNICODE ?: REACTION_TEXT}")
        val message = event.retrieveMessage().complete()
        val top500optional =
            message.reactions.stream().filter { it.reactionEmote.isEmote && it.reactionEmote.emote.name == raw }
                .findFirst()
        if (top500optional.isPresent) {
            logger.trace("--Count is $top500optional; wanted count is $REACTION_COUNT")
            if (top500optional.get().count == REACTION_COUNT) {
                addToTheGarbageMuseum(event, message)
            }
        }
    }

    private fun addToTheGarbageMuseum(event: MessageReactionAddEvent, message: Message) {
        val quote = message.contentDisplay
        val author = message.author
        val authorUsername = author.name
        val authorAvatar = author.avatarUrl
        val quoteTimestamp = message.timeCreated
        val authorAvatarDominantColor = ColorUtils.getDominant(BufferedInputStream(URL(authorAvatar).openStream()))

        print("Adding a quote from $authorUsername to the wall of fame ($quote).")

        val link = "https://discordapp.com/channels" + "/${event.guild.id}" + "/${event.channel.id}" + "/${message.id}"
        val embed = EmbedBuilder()
            .setColor(authorAvatarDominantColor)
            .setAuthor(authorUsername, link, authorAvatar)
            .setDescription(quote)
            .setTimestamp(quoteTimestamp)
            .build()

        event.jda.getTextChannelById(GARBAGE_MUSEUM)!!.sendMessageEmbeds(embed).complete()
    }

}

