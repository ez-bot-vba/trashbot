package com.bancarelvalentin.trashbot.background.garbagequotes


import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.background.BackgroundProcess


class GarbageQuoteBot : BackgroundProcess() {
    override val rawDesc = "Save best punchline to a hall of fame"
    override val rawName = "Garbage quotes"

    override fun getListeners(): Array<EventListener> {
        return arrayOf(OnGarbageQuoteOnReaction(this))
    }
}